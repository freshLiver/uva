#include <bits/stdc++.h>

using namespace std;

#define N_TOLL 24

typedef struct RECORD {
    int startTime;
    int enterKM, exitKM;

    RECORD(int enter, int exit) : enterKM(enter), exitKM(exit) {}
    RECORD(int start, int enter, int exit) : startTime(start), enterKM(enter), exitKM(exit) {}
} Record;

static inline int ParseTime(string time) {
    // input format should be MM:DD:hh:mm
    time.erase(8, 1);
    time.erase(5, 1);
    time.erase(2, 1);
    return stoi(time);
}

bool CompareRecord(pair<string, Record> a, pair<string, Record> b) {
    //
    return a.second.startTime < b.second.startTime;
}

bool CompareLicense(pair<string, Record> a, pair<string, Record> b) {

    // name to lowercase
    string nameA = a.first, nameB = b.first;
    for (char &c : nameA)
        if ('a' <= c && c <= 'z')
            c += 'A' - 'a';
    for (char &c : nameB)
        if ('a' <= c && c <= 'z')
            c += 'A' - 'a';

    return nameA.compare(nameB) < 0;
}

int main(int argc, char const *argv[]) {
    /* code */
    int nCase;
    cin >> nCase;

    for (int iCase = 0; iCase < nCase; ++iCase) {

        // toll of each hour
        int tolls[N_TOLL];
        for (int iToll = 0; iToll < N_TOLL; ++iToll)
            cin >> tolls[iToll];

        // discard '\n'
        scanf("%*c");

        // get photo records
        vector<pair<string, Record>> enterRecords, exitRecords;

        bool isEnter;
        int time, loc_km;
        string line, license;
        for (size_t pos; getline(cin, line);) {
            // no more photo records
            if (line == "")
                break;

            // check if this car in record
            pos = line.find(" ");
            license = line.substr(0, pos);
            line.erase(0, pos + 1);

            pos = line.find(" ");
            time = ParseTime(line.substr(0, pos));
            line.erase(0, pos + 1);

            isEnter = line[1] == 'n';
            line.erase(0, line.find(" ") + 1);

            loc_km = stoi(line);

            // record all photo and time for sorting after
            if (isEnter)
                enterRecords.push_back(make_pair(license, Record(time, loc_km, -1)));
            else
                exitRecords.push_back(make_pair(license, Record(time, -1, loc_km)));
        }

        // sort records by start time
        sort(enterRecords.begin(), enterRecords.end(), CompareRecord);
        sort(exitRecords.begin(), exitRecords.end(), CompareRecord);

        // using exit records update enter records
        for (auto exit : exitRecords) {
            // find same license
            for (auto &enter : enterRecords) {
                // if same license and exitKM is blank and enter before exit => update this record
                if (!enter.first.compare(exit.first) && (enter.second.exitKM == -1) && (enter.second.startTime < exit.second.startTime)) {
                    enter.second.exitKM = exit.second.exitKM;
                    break;
                }
            }
        }

        // sort result in alphabet order
        sort(enterRecords.begin(), enterRecords.end(), CompareLicense);

        // print toll
        for (auto record : enterRecords) {

            // if exit is blank, pass
            if (record.second.exitKM == -1)
                continue;

            // if (!record.first.compare("AY"))
            //     cout << endl;

            double dollar = 3;
            int unitToll = tolls[record.second.startTime % 10000 / 100];
            dollar += unitToll * abs(record.second.exitKM - record.second.enterKM) / 100.0;

            printf("%s $%.2f\n", record.first.c_str(), dollar);
            if (iCase != nCase - 1)
                cout << endl;
        }
    }
    return 0;
}
