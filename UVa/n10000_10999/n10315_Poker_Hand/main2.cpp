#include <bits/stdc++.h>

using namespace std;

#define StraightFlush 7
#define Four 6
#define Full 5
#define Flush 4
#define Straight 3
#define Three 2
#define TwoPair 1
#define Two 0
#define High -1

inline int GetValue(char number, char suit);
inline pair<int, vector<int>> ExtractLargerPair(int sortedValues[], int len);
inline int CompareSingles(int sortedSingleA[], int sortedSingleB[], int len);
tuple<int, vector<int>, vector<int>> GetBestForm(int sorted[]);

int iCase = 0;

void printResult(int result) {
    if (result > 0)
        printf("Black wins.\n");
    else if (result < 0)
        printf("White wins.\n");
    else
        printf("Tie.\n");
}


int main(int argc, char const *argv[]) {
    // repeat until EOF
    string line;
    while (getline(std::cin, line)) {

        ++iCase;

        // parse this line
        int blacks[5] = {0};
        int whites[5] = {0};

        for (int i = 0; i < 5; ++i)
            blacks[i] = GetValue(line[3 * i], line[3 * i + 1]);
        for (int i = 5; i < 10; ++i)
            whites[i - 5] = GetValue(line[3 * i], line[3 * i + 1]);

        // sort values
        sort(blacks, blacks + 5);
        sort(whites, whites + 5);

        // print result
        auto resB = GetBestForm(blacks);
        auto resW = GetBestForm(whites);


        // print result
        if (iCase == 59)
            int i = 0;

        int result;
        if (get<0>(resB) > get<0>(resW))
            result = 1;

        else if (get<0>(resB) < get<0>(resW))
            result = -1;

        else {

            int mode = get<0>(resB);

            //
            // single
            //
            if (mode == High || mode == Flush)
                result = CompareSingles(&get<1>(resB)[0], &get<1>(resW)[0], 5);

            //
            // 1 or 2 pair
            //
            else if (mode == Two || mode == TwoPair) {
                // compare LARGER pair
                result = get<1>(resB)[0] - get<1>(resW)[0];

                // same value
                if (result == 0) {
                    // if 2 pair, compare SMALLER pair
                    if (mode == TwoPair)
                        result = get<1>(resB)[1] - get<1>(resW)[1];

                    // 1 pair or SMALLER pair still same, compare singles
                    if (result == 0)
                        result = CompareSingles(&get<2>(resB)[0], &get<2>(resW)[0], mode == Two ? 3 : 1);
                }
            }
            //
            // others (straight, four, flush, full, three) must diff value
            //
            else
                result = get<1>(resB)[0] - get<1>(resW)[0];
        }


        printResult(result);
    }
    return 0;
}


// ----------------------------------------


tuple<int, vector<int>, vector<int>> GetBestForm(int sorted[]) {

    vector<int> sortedValues;
    for (int i = 0; i < 5; ++i)
        sortedValues.push_back(sorted[i] / 10);

    vector<int> compareValues;
    vector<int> remainValues;

    //
    // check straight and flush
    //
    bool isStraight = true, isFlush = true;
    for (int i = 1; i < 5 && isStraight; ++i)
        if (sortedValues[i] != sortedValues[i - 1] + 1)
            isStraight = false;

    for (int i = 1; i < 5; ++i)
        if ((sorted[i - 1] % 10) != (sorted[i] % 10))
            isFlush = false;


    //
    // straight flush or straight
    //
    if (isStraight) {
        compareValues.push_back(sortedValues.back());
        return make_tuple(isFlush ? StraightFlush : Straight, compareValues, remainValues);
    }

    // num of most larger repeated "times, value"
    auto mostRepeated = make_pair(0, 0);
    for (int i = 0; i < 5; ++i) {
        // repeat times of this value
        int sameCount = 0;
        for (int j = 0; j < 5; ++j)
            if (sortedValues[j] == sortedValues[i])
                ++sameCount;

        if (sameCount >= mostRepeated.first)
            mostRepeated = make_pair(sameCount, sortedValues[i]);
    }


    // remains value of sortedValues without most repeat value
    for (int i = 0, count = 0; i < 5; ++i)
        if (sortedValues[i] != mostRepeated.second)
            remainValues.push_back(sortedValues[i]);


    //
    // four
    //
    compareValues.push_back(mostRepeated.second);

    if (mostRepeated.first == 4) {
        return make_tuple(Four, compareValues, remainValues);
    }

    //
    // full house or three
    //
    else if (mostRepeated.first == 3) {
        if (remainValues[0] == remainValues[1])
            return make_tuple(Full, compareValues, remainValues);
        return make_tuple(Three, compareValues, remainValues);
    }
    //
    // 1 or 2 pair
    //
    else if (mostRepeated.first == 2) {
        // try to extract pair
        auto smallPair = ExtractLargerPair(&remainValues[0], remainValues.size());

        // 2 pair
        if (smallPair.first != -1) {
            compareValues.push_back(remainValues[1]);
            return make_tuple(TwoPair, compareValues, smallPair.second);
        }
        return make_tuple(Two, compareValues, smallPair.second);
    }

    //
    // flush
    //
    else if (isFlush)
        return make_tuple(Flush, sortedValues, sortedValues);
    //
    // single
    //
    return make_tuple(High, sortedValues, sortedValues);
}


// ----------------------------------------

inline int CompareSingles(int sortedSingleA[], int sortedSingleB[], int len) {
    int compare = 0;
    for (int i = len - 1; i >= 0 && compare == 0; --i) {
        if (sortedSingleA[i] > sortedSingleB[i])
            compare = 1;
        else if (sortedSingleA[i] < sortedSingleB[i])
            compare = -1;
    }
    return compare;
}

inline pair<int, vector<int>> ExtractLargerPair(int sortedValues[], int len) {

    bool isSingle[len];
    for (int i = 0; i < len; ++i)
        isSingle[i] = 1;


    int pairValue = -1;
    for (int i = len - 1; i > 0; --i)
        if (sortedValues[i] == sortedValues[i - 1]) {
            isSingle[i] = 0;
            isSingle[i - 1] = 0;
            pairValue = sortedValues[i];
            break;
        }

    vector<int> remains;
    for (int i = 0; i < len; ++i)
        if (isSingle[i])
            remains.push_back(sortedValues[i]);

    return make_pair(pairValue, remains);
}

inline int GetValue(char number, char suit) {

    int value = 0;
    if ('2' <= number && number <= '9')
        value = 10 * (number - '0');
    else if (number == 'T')
        value = 100;
    else if (number == 'J')
        value = 110;
    else if (number == 'Q')
        value = 120;
    else if (number == 'K')
        value = 130;
    else
        value = 140;


    if (suit == 'S')
        value += 4;
    else if (suit == 'H')
        value += 3;
    else if (suit == 'D')
        value += 2;
    else
        value += 1;

    return value;
}