#include <bits/stdc++.h>

using namespace std;


int main(int argc, char const *argv[]) {

    string line;
    vector<int> nums;
    vector<pair<int, int>> numMap;

    for (int i = 0; getline(cin, line); ++i) {

        // get nums from this line
        for (size_t pos = 0; (pos = line.find(" ")) != string::npos;) {
            if (pos != 0) {
                int num = stoi(line.substr(0, pos));
                nums.push_back(num);
            }
            line.erase(0, pos + 1);
        }
        if (line != "")
            nums.push_back(stoi(line));

        // check each num in this line
        for (int num : nums) {
            // check if this num in numMap
            bool notFound = true;
            for (auto iter = numMap.begin(); notFound && iter != numMap.end(); ++iter)
                if (iter->first == num) {
                    notFound = false;
                    iter->second++;
                }
            if (notFound)
                numMap.push_back(make_pair(num, 1));
        }
        nums.clear();
    }

    for (auto pair : numMap)
        cout << pair.first << " " << pair.second << endl;

    return 0;
}
