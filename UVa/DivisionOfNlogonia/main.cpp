#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    for (;;) {
        int k;
        scanf("%d", &k);
        if (k == 0)
            return 0;

        // get division pt
        int dx, dy;
        scanf("%d", &dx);
        scanf("%d", &dy);

        // input k resident pts
        for (int i = 0; i < k; ++i) {
            int x, y;
            scanf("%d", &x);
            scanf("%d", &y);

            x -= dx;
            y -= dy;

            if (x > 0 && y > 0)
                printf("NE\n");
            else if (x < 0 && y > 0)
                printf("NO\n");
            else if (x < 0 && y < 0)
                printf("SO\n");
            else if (x > 0 && y < 0)
                printf("SE\n");
            else
                printf("divisa\n");
        }
    }


    return 0;
}
