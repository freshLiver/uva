#include <bits/stdc++.h>

using namespace std;

const char *suits[] = {" of Clubs", " of Diamonds", " of Hearts", " of Spades"};
const char *values[] = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};

inline void printHand(int hand[]) {
    for (int i = 0; i < 52; ++i) {
        int suit = (hand[i] - 1) / 13;
        int value = (hand[i]) % 13;
        printf("%s%s\n", values[value], suits[suit]);
        // printf("%d\n", hand[i]);
    }
}

int main(int argc, char const *argv[]) {

    // num of test cases
    int nCase;
    scanf("%d", &nCase);

    for (int iCase = 0; iCase < nCase; ++iCase) {

        int nMethod;
        scanf("%d", &nMethod);

        // input all methods
        int methods[nMethod][53];
        for (int n = 0; n < nMethod; ++n)
            for (int pos = 1; pos < 53; ++pos)
                scanf("%d", &methods[n][pos]);
        // read newline
        scanf("%*c");


        // init default cards
        int *currHand = new int[53];
        int *nextHand = new int[53];

        for (int i = 1; i < 53; ++i)
            currHand[i] = i;

        // do method until empty line
        string line;

        bool caseFin = false;
        while (!caseFin && getline(std::cin, line)) {
            if (line == "")
                caseFin = true;
            else {
                // get method id
                int mId = stoi(line) - 1;

                // apply this method
                for (int i = 1; i < 53; ++i)
                    nextHand[i] = currHand[methods[mId][i]];

                // apply change
                int *tmp = currHand;
                currHand = nextHand;
                nextHand = tmp;
            }
        }

        // print result (one card per line)
        printHand(currHand + 1);
        if (iCase != nCase - 1)
            printf("\n");
    }

    return 0;
}
