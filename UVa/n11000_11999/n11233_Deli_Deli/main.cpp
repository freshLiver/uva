#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    int nIrr, nVoc;

    cin >> nIrr >> nVoc;

    // input irregular list
    string single[nIrr], plural[nIrr];
    for (int i = 0; i < nIrr; ++i)
        cin >> single[i] >> plural[i];


    // input and output plural form
    for (int i = 0; i < nVoc; ++i) {

        // input voc
        string voc;
        cin >> voc;

        // if irr
        bool done = false;
        for (int j = 0; j < nIrr && !done; ++j)
            if (!strcmp(voc.c_str(), single[j].c_str())) {
                cout << plural[j] << endl;
                done = true;
            }
        if (done)
            continue;
        else {
            char last2 = voc[voc.length() - 2];
            char last1 = voc[voc.length() - 1];

            // if end with y -> ies
            if (last1 == 'y' && !(last2 == 'a' || last2 == 'e' || last2 == 'i' || last2 == 'o' || last2 == 'u'))
                cout << voc.substr(0, voc.length() - 1) << "ies" << endl;

            // if end with o, s, ch, sh, x -> es
            else if (last1 == 'o' || last1 == 's' || last1 == 'x')
                cout << voc << "es" << endl;
            else if (last1 == 'h' && (last2 == 'c' || last2 == 's'))
                cout << voc << "es" << endl;
            // else append s
            else
                cout << voc << 's' << endl;
        }
    }

    return 0;
}
