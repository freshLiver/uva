#include <bits/stdc++.h>

#define PREFIX "LOGGER :: "

#define log( fmt, ... ) logger( PREFIX, "", fmt, ##__VA_ARGS__ )
#define log_nl( fmt, ... ) logger( PREFIX, "\n", fmt, ##__VA_ARGS__ )

#define logger( prefix, postfix, fmt, ... ) printf( "%s" fmt "%s", prefix, ##__VA_ARGS__, postfix )

using namespace std;

int sizeX = 0, sizeY = 0;

void SaveFile( char **file, string filename );
void InitFile( char **file, int m, int n );
void Clear( char **file );
void ColorAll( char **file, char color );
void ColorVertical( char **file, int V, int Hs, int Hf, char color );
void ColorHorizontal( char **file, int H, int Vs, int Vf, char color );
void ColorRectangle( char **file, int Hs, int Hf, int Vs, int Vf, char color );
void FillColor( char **file, int x, int y, char color );

int main( int argc, char const *argv[] ) {

    string tmp;

    // getline until eof
    while ( getline( std::cin, tmp ) ) {

        // tokenize this line
        vector<string> tokens;
        stringstream ss( tmp );
        for ( string token; getline( ss, token, ' ' ); )
            tokens.push_back( token );

        // parse and do command
        char **file;

        if ( tokens[0] == "I" )
            InitFile( file, stoi( tokens[1] ), stoi( tokens[2] ) );
        else if ( tokens[0] == "C" )
            Clear( file );
        else if ( tokens[0] == "L" )
            ColorAll( file, tokens[1][0] );
        else if ( tokens[0] == "V" )
            ColorVertical( file, stoi( tokens[1] ), stoi( tokens[2] ), stoi( tokens[3] ), tokens[4][0] );
        else if ( tokens[0] == "H" )
            ColorHorizontal( file, stoi( tokens[1] ), stoi( tokens[2] ), stoi( tokens[3] ), tokens[4][0] );
        else if ( tokens[0] == "K" )
            ColorRectangle( file, stoi( tokens[1] ), stoi( tokens[2] ), stoi( tokens[3] ), stoi( tokens[4] ), tokens[5][0] );
        else if ( tokens[0] == "F" )
            FillColor( file, stoi( tokens[1] ), stoi( tokens[2] ), tokens[3][0] );
        else if ( tokens[0] == "S" )
            SaveFile( file, tokens[1] );
        else if ( tokens[0] == "X" )
            break;
        else {
            log_nl( "Should Not Be Here." );
            return -1;
        }
    }


    return 0;
}

void SaveFile( char **file, string filename ) {
    // print file name
    cout << filename << endl;

    // print content
    for ( int i = 0; i < sizeY; ++i ) {
        for ( int j = 0; j < sizeX; ++j )
            printf( "%c", file[i][j] );
        printf( "\n" );
    }
}


void InitFile( char **file, int row, int col ) {

    // set global size
    sizeX = row;
    sizeY = col;

    // new a file
    file = new char *[col];
    for ( int i = 0; i < col; ++i )
        file[i] = new char[row];

    // clear content
    Clear( file );
}

void Clear( char **file ) { ColorAll( file, '0' ); }

void ColorAll( char **file, char color ) {
    // set all pixels to color
    for ( int i = 0; i < sizeY; ++i )
        for ( int j = 0; j < sizeX; ++j )
            file[i][j] = color;
}

void ColorVertical( char **file, int V, int Hs, int Hf, char color ) {
    for ( int i = Hs; i <= Hf; ++i )
        file[i][V] = color;
}

void ColorHorizontal( char **file, int Vs, int Vf, int H, char color ) {
    for ( int i = Vs; i <= Vf; ++i )
        file[H][i] = color;
}

void ColorRectangle( char **file, int Vs, int Vf, int Hs, int Hf, char color ) {
    for ( int i = Hs; i <= Hf; ++i )
        for ( int j = Vs; j <= Vf; ++j )
            file[i][j] = color;
}


void FillColor( char **file, int x, int y, char color ) {

    set<char *> range;
    char rangeColor = file[y][x];

    // TODO find all neighbor with same color
    // https://en.wikipedia.org/wiki/Connected-component_labeling
    range.insert( &file[y][x] );



    // fill all pixel in set:range with color
    for ( auto iter = range.begin(); iter != range.end(); iter++ )
        **iter = color;
}