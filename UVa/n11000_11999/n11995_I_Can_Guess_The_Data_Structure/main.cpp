#include <bits/stdc++.h>

#define PUT 1
#define TAKE 2

using namespace std;

typedef struct POSSIBILITY {
    bool s = true;
    bool q = true;
    bool pq = true;
} Possibility;


int main(int argc, char const *argv[]) {

    //
    int size;
    while (EOF != scanf("%d", &size)) {

        stack<int> s;
        queue<int> q;
        priority_queue<int> pq;

        Possibility poss;

        for (int i = 0; i < size; ++i) {
            int cmd, value;
            scanf("%d %d", &cmd, &value);

            if (cmd == PUT) {
                s.push(value);
                q.push(value);
                pq.push(value);
            }
            else {
                int s_res = s.top();
                int q_res = q.front();
                int pq_res = pq.top();

                s.pop();
                q.pop();
                pq.pop();

                if (poss.s)
                    if (value != s_res)
                        poss.s = false;

                if (poss.q)
                    if (value != q_res)
                        poss.q = false;

                if (poss.pq)
                    if (value != pq_res)
                        poss.pq = false;
            }
        }

        // check result
        if ((poss.pq + poss.q + poss.s) == 0)
            printf("impossible\n");
        else if ((poss.pq + poss.q + poss.s) > 1)
            printf("not sure\n");
        else if (poss.s)
            printf("stack\n");
        else if (poss.q)
            printf("queue\n");
        else if (poss.pq)
            printf("priority queue\n");
    }


    return 0;
}
