#include <bits/stdc++.h>

using namespace std;

int TimeString2Value(char h1, char h0, char m1, char m0) {

    int value = m0 - '0';
    value += 10 * (m1 - '0');
    value += 60 * (h0 - '0');
    value += 600 * (h1 - '0');

    return value;
}

bool CompareInterval(pair<int, int> a, pair<int, int> b) {
    if (a.first != b.first)
        return a.first < b.first;
    return a.second < b.second;
}

int main(int argc, char const *argv[]) {

    string line;
    for (int iDay = 1; getline(cin, line); ++iDay) {
        int nInterval = stoi(line);

        vector<pair<int, int>> intervals;
        for (int i = 0; i < nInterval; ++i) {
            getline(cin, line);

            // parse this line
            int start = TimeString2Value(line[0], line[1], line[3], line[4]);
            int end = TimeString2Value(line[6], line[7], line[9], line[10]);

            intervals.push_back(make_pair(start, end));
        }

        // add <10:00,10:00> and <18:00,18:00>
        intervals.push_back(make_pair(600, 600));
        intervals.push_back(make_pair(1080, 1080));

        // sort intervals
        sort(intervals.begin(), intervals.end(), CompareInterval);

        // remore pure overlap
        vector<pair<int, int>> no_overlap;
        for (auto interval : intervals) {
            if (!no_overlap.empty()) {
                auto back = no_overlap.back();
                if (back.first <= interval.first && interval.second <= back.second)
                    continue;
            }
            no_overlap.push_back(interval);
        }


        // find longest nap<start, length>
        pair<int, int> nap(0, 0);
        for (int i = 0; i < no_overlap.size() - 1; ++i) {
            int length = no_overlap[i + 1].first - no_overlap[i].second;
            if (length > nap.second)
                nap = make_pair(no_overlap[i].second, length);
        }

        // print longest nap
        int hour = nap.second / 60, minute = nap.second % 60;
        printf("Day #%d: ", iDay);
        printf("the longest nap starts at %02d:%02d and will last for", nap.first / 60, nap.first % 60);
        if (hour)
            printf(" %d hours and", hour);
        printf(" %d minutes.\n", minute);
    }
    return 0;
}
