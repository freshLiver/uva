#include <bits/stdc++.h>

using namespace std;

typedef struct S_Job {
    int index;
    double time, pay;
} Job;

int CompareTP(const void *a, const void *b) {
    double aTP = ((Job *)a)->pay / ((Job *)a)->time;
    double bTP = ((Job *)b)->pay / ((Job *)b)->time;

    if (aTP < bTP)
        return 1;
    else if (aTP == bTP)
        return 0;
    return -1;
}

int main(int argc, char const *argv[]) {

    int nCase;
    cin >> nCase;

    for (int iCase = 0; iCase < nCase; ++iCase) {

        int nJobs;
        cin >> nJobs;

        // get time and pay of each job
        Job jobs[nJobs];
        for (int iJob = 0; iJob < nJobs; ++iJob) {
            jobs[iJob].index = iJob + 1;
            cin >> jobs[iJob].time >> jobs[iJob].pay;
        }

        // sort jobs
        qsort(jobs, nJobs, sizeof(Job), CompareTP);

        // print jobs' index
        if ( iCase != 0 )
            cout << endl;
        for (int iJobs = 0; iJobs < nJobs - 1; ++iJobs)
            cout << jobs[iJobs].index << " ";
        cout << jobs[nJobs - 1].index << "\n";
    }

    return 0;
}
