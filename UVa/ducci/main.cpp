#include <bits/stdc++.h>

using namespace std;

inline bool allZero(int *arr, int size) {
    for (int i = 0; i < size; ++i)
        if (arr[i] != 0)
            return false;
    return true;
}

inline bool same(int *a, int *b, int size) {
    for (int i = 0; i < size; ++i)
        if (a[i] != b[i])
            return false;
    return true;
}

inline bool repeat(vector<int *> visited, int *a, int size) {
    for (auto iter = visited.begin(); iter != visited.end(); ++iter)
        if (same(*iter, a, size))
            return true;
    return false;
}


int main(int argc, char const *argv[]) {

    // how many test data
    int total;
    scanf("%d", &total);

    for (int iData = 0; iData < total; ++iData) {

        // how many numbers in this test data
        int size;
        scanf("%d", &size);

        // input $size numbers and save to array

        int value[size];
        for (int i = 0; i < size; ++i)
            scanf("%d", &value[i]);

        // check ducci loop
        vector<int *> visited;
        visited.push_back(value);

        bool end = false;
        while (!end) {

            // calc new value
            int *newValue = new int[size];

            for (int index = 0; index < size - 1; ++index)
                newValue[index] = abs(value[index] - value[index + 1]);
            newValue[size - 1] = abs(value[size - 1] - value[0]);

            if (allZero(newValue, size)) {
                printf("ZERO\n");
                end = true;
            }
            else if (repeat(visited, newValue, size)) {
                printf("LOOP\n");
                end = true;
            }
            else {
                visited.push_back(newValue);
                for (int index = 0; index < size; ++index)
                    value[index] = newValue[index];
            }
        }
    }

    return 0;
}
