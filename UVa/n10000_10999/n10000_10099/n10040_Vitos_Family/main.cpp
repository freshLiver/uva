#include <bits/stdc++.h>

using namespace std;

int compareInt(const void *a, const void *b) {

    int numA = *(int *)a, numB = *(int *)b;

    if (numA > numB)
        return 1;
    else if (numA == numB)
        return 0;
    return -1;
}

int main(int argc, char const *argv[]) {

    int nCase = 0;
    cin >> nCase;

    for (int iCase = 0; iCase < nCase; ++iCase) {

        // how many relatives
        int nRelatives;
        cin >> nRelatives;

        // get each relatives pos
        int relatives[nRelatives];
        for (int iRelatives = 0; iRelatives < nRelatives; ++iRelatives)
            cin >> relatives[iRelatives];


        // find middle point of all relatives
        qsort(relatives, nRelatives, sizeof(int), compareInt);

        // odd
        if (nRelatives % 2) {
            int middle = relatives[nRelatives / 2];

            // count dis sum to all relatives
            int disSum = 0;
            for (int rela : relatives)
                disSum += abs(rela - middle);

            cout << disSum << endl;
        }
        // even
        else {
            int middleL = relatives[nRelatives / 2 - 1];
            int middleR = relatives[nRelatives / 2];

            int disSumL = 0, disSumR = 0;
            for (int rela : relatives) {
                disSumL += abs(rela - middleL);
                disSumR += abs(rela - middleR);
            }

            cout << min(disSumL, disSumR) << endl;
        }
    }

    return 0;
}
