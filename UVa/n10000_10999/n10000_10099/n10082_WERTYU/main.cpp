#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    // clang-format off
    char groups[256];
    groups['`'] = '`'; groups['Q'] = 'Q'; groups['A'] = 'A'; groups['Z'] = 'Z';
    groups['1'] = '`'; groups['W'] = 'Q'; groups['S'] = 'A'; groups['X'] = 'Z';
    groups['2'] = '1'; groups['E'] = 'W'; groups['D'] = 'S'; groups['C'] = 'X';
    groups['3'] = '2'; groups['R'] = 'E'; groups['F'] = 'D'; groups['V'] = 'C';
    groups['4'] = '3'; groups['T'] = 'R'; groups['G'] = 'F'; groups['B'] = 'V';
    groups['5'] = '4'; groups['Y'] = 'T'; groups['H'] = 'G'; groups['N'] = 'B';
    groups['6'] = '5'; groups['U'] = 'Y'; groups['J'] = 'H'; groups['M'] = 'N';
    groups['7'] = '6'; groups['I'] = 'U'; groups['K'] = 'J'; groups[','] = 'M';
    groups['8'] = '7'; groups['O'] = 'I'; groups['L'] = 'K'; groups['.'] = ',';
    groups['9'] = '8'; groups['P'] = 'O'; groups[';'] = 'L'; groups['/'] = '.';
    groups['0'] = '9'; groups['['] = 'P'; groups['\''] = ';';
    groups['-'] = '0'; groups[']'] = '['; 
    groups['='] = '-'; groups['\\'] = ']'; 
    groups[' '] = ' ';
    // clang-format on

    int count = 0;
    for (string line; getline(cin, line); ++count) {

        if (count)
            cout << endl;

        // parse this line
        for (char ch : line) {
            char out = groups[ch];
            cout << out;
        }
    }
    return 0;
    // UVa WA
}
