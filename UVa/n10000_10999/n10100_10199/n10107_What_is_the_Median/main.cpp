#include <bits/stdc++.h>

using namespace std;

typedef long long LL;

int main(int argc, char const *argv[]) {

    int size = 0;
    vector<LL> nums;

    for (LL num; cin >> num;) {
        // insertion sort
        bool found = false;
        for (auto iter = nums.begin(); !found && iter != nums.end(); ++iter)
            if (found = (num >= *iter))
                nums.insert(iter, num);
        if (!found)
            nums.push_back(num);
        ++size;


        // print median
        if (size % 2)
            cout << nums[size / 2] << endl;
        else
            cout << (nums[size / 2 - 1] + nums[size / 2]) / 2 << endl;
    }

    return 0;
}
