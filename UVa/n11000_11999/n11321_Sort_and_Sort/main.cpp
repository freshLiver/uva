#include <bits/stdc++.h>

using namespace std;

bool CompareAsc(int a, int b) { return a < b; }

bool CompareDes(int a, int b) { return b < a; }


int main(int argc, char const *argv[]) {

    while (1) {

        // get nNums, modNum
        int nNums, modNum;
        cin >> nNums >> modNum;

        // no more case
        if (!nNums || !modNum) {
            cout << "0 0" << endl;
            break;
        }

        // build a oddVector and evenVector
        vector<int> odds, evens;

        // input all nums
        for (int i = 0, tmp; i < nNums; ++i) {
            cin >> tmp;

            if (tmp % 2)
                odds.push_back(tmp);
            else
                evens.push_back(tmp);
        }

        // sort odds in descend order, evens in ascend order
        sort(evens.begin(), evens.end(), CompareAsc);
        sort(odds.begin(), odds.end(), CompareDes);

        // build $modNum vector for all mod values
        // mod num range: [-(modNum-1),(modNum+1)]

        int shift = modNum - 1;
        int size = modNum * 2 + 1;

        vector<int> *remains[size];
        for (int i = 0; i < size; ++i)
            remains[i] = new vector<int>();

        // push odds to remains first
        for (int odd : odds) {
            int index = odd >= 0 ? odd % modNum : odd - (odd / modNum) * modNum;
            remains[index + shift]->push_back(odd);
        }

        // push evens
        for (int even : evens) {
            int index = even >= 0 ? even % modNum : even - (even / modNum) * modNum;
            remains[index + shift]->push_back(even);
        }

        // print sort result
        cout << nNums << " " << modNum << endl;
        for (vector<int> *remain : remains)
            for (int num : *remain)
                cout << num << endl;
    }

    return 0;
}
