#include <bits/stdc++.h>

using namespace std;

inline vector<int> SplitAllInt(string &str) {

    vector<int> nums;
    for (size_t pos; (pos = str.find(" ")) != string::npos;) {
        nums.push_back(stoi(str.substr(0, pos)));
        str.erase(0, pos + 1);
    }
    nums.push_back(stoi(str));

    return nums;
}

inline void FlipTo(vector<int> &nums, int end) {
    for (int s = 0, e = end - 1; s < e && s < nums.size(); ++s, --e) {
        int tmp = nums[s];
        nums[s] = nums[e];
        nums[e] = tmp;
    }
}

inline int FindMaxPos(vector<int> &nums, int end) {
    int pos = 0, maxNum = INT32_MIN;
    for (int i = 0; i < end; ++i)
        if (nums[i] > maxNum) {
            maxNum = nums[i];
            pos = i;
        }
    return pos;
}

int main(int argc, char const *argv[]) {

    for (string line; getline(cin, line);) {
        // get all int from this line
        cout << line << endl;
        vector<int> nums = SplitAllInt(line);

        // flip to end at most N times
        for (int i = 0; i < nums.size(); ++i) {

            // get max Num pos
            int len = nums.size() - i;
            int pos = FindMaxPos(nums, len);

            // what should do in each situation
            if (pos == len - 1)
                continue;

            if (pos != 0) {
                FlipTo(nums, pos + 1);
                cout << nums.size() - pos << " ";
            }
            FlipTo(nums, len);
            cout << i + 1 << " ";
        }
        cout << "0\n";
    }
    return 0;
}
