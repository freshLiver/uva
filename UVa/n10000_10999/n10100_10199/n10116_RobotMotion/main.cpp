#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    // get 3 int : rows, cols, init
    int rows, cols, init;

    while (true) {

        scanf("%d %d %d", &rows, &cols, &init);
        if (!rows || !cols || !init)
            break;

        // bool array for check loop
        vector<tuple<int, int>> visited;

        char direct[rows][cols];

        // scanf all paths
        for (int r = 0; r < rows; ++r) {

            char line[cols];
            scanf("%s", line);

            for (int c = 0; c < cols; ++c)
                direct[r][c] = line[c];
        }

        // start walking until exit or loop
        bool loop = false;
        int row = 0, col = init - 1;
        while (!loop && (0 <= row && row < rows) && (0 <= col && col < cols)) {

            tuple<int, int> now = make_tuple(row, col);

            // check if this pos already visited
            for (int i = 0; !loop && i < visited.size(); ++i)
                if (visited.at(i) == now) {
                    printf("%lu step(s) before a loop of %d step(s)\n", i, visited.size() - i);
                    loop = true;
                }

            // if not visited, add it and goto next pos
            if (!loop) {
                visited.push_back(now);

                // get next direct
                char next = direct[row][col];
                if (next == 'S')
                    row += 1;
                else if (next == 'N')
                    row -= 1;
                else if (next == 'E')
                    col += 1;
                else
                    col -= 1;
            }
        }

        if (!loop)
            printf("%lu step(s) to exit\n", visited.size());
    }
    return 0;
}
