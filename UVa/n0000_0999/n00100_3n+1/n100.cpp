#include <cstdio>


int main( int argc, char const *argv[] ) {

    int i, j;

    while ( EOF != scanf( "%d %d", &i, &j ) ) {

        int start, end;
        if ( i > j ) {
            start = j;
            end = i;
        }
        else {
            start = i;
            end = j;
        }


        int max = 0;

        for ( int i = start; i <= end; ++i ) {
            int value = i;
            int count = 1;

            while ( value != 1 ) {
                if ( value & 1 )
                    value = value * 3 + 1;
                else
                    value = value / 2;
                ++count;
            }

            // check current max
            if ( count > max )
                max = count;
        }

        printf( "%d %d %d\n", i, j, max );
    }
    return 0;
}
