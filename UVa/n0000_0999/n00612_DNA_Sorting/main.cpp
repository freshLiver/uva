#include <bits/stdc++.h>

using namespace std;

bool CompareSortedness(string a, string b) {
    int sortednessA = 0, sortednessB = 0;

    for (int i = 0; i < a.length(); ++i)
        for (int j = i + 1; j < a.length(); ++j)
            if (a[i] > a[j])
                ++sortednessA;

    for (int i = 0; i < b.length(); ++i)
        for (int j = i + 1; j < b.length(); ++j)
            if (b[i] > b[j])
                ++sortednessB;

    return sortednessB > sortednessA;
}

int main(int argc, char const *argv[]) {

    int nCase;
    cin >> nCase;

    for (int iCase = 0; iCase < nCase; ++iCase) {

        int num, len;
        cin >> len >> num;

        // input $num DNS
        string dna;
        vector<string> DNA;
        for (int iDNA = 0; iDNA < num; ++iDNA) {
            cin >> dna;
            DNA.push_back(dna);
        }

        // sort and print result
        std::stable_sort(DNA.begin(), DNA.end(), CompareSortedness);
        for (string dna : DNA)
            cout << dna << endl;

        if (iCase != nCase - 1)
            cout << endl;
    }
    return 0;
}
