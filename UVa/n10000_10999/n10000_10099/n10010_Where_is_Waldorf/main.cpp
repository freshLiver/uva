#include <bits/stdc++.h>

using namespace std;

void FindWordInGrids(vector<string> grids, string &word) {

    // build a matrix
    int nGrids = grids.size();
    int lGrids = grids.at(0).length();
    int tags[nGrids][lGrids];

    // tag all letter in word
    for (int row = 0; row < nGrids; ++row) {
        for (int col = 0; col < lGrids; ++col) {
            tags[row][col] = -1;

            for (int iChar = 0; iChar < word.length(); ++iChar) {
                if (grids[row][col] == word[iChar]) {
                    tags[row][col] = iChar;
                    break;
                }
            }
        }
    }

    // find word in 
}

int main(int argc, char const *argv[]) {

    int nCase;
    cin >> nCase;

    for (int iCase = 0; iCase < nCase; ++iCase) {
        // input n, m
        int nGrid, lGrid;
        cin >> nGrid >> lGrid;

        // check n grid of letters
        string line;
        vector<string> grids;
        for (int iGrid = 0; iGrid < nGrid; ++iGrid) {
            getline(cin, line);
            grids.push_back(line);
        }


        // find each word in the letter matrix
        int nTest;
        cin >> nTest;
        for (int i = 0; i < nTest; ++i) {
            getline(cin, line);
            FindWordInGrids(grids, line);
        }
    }

    return 0;
}
