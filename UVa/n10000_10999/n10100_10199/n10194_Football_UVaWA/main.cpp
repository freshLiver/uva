#include <bits/stdc++.h>

using namespace std;

typedef struct TEAM_INFO {
    int n = 0, point = 0;
    int win = 0, lose = 0, ties = 0;
    int earn = 0, give = 0, diff = 0;

} TeamInfo;

static void LogGame(map<string, TeamInfo> &teams, string name, int earn, int give) {
    auto team = teams.find(name);
    int diff = earn - give;

    if (diff > 0) {
        team->second.win++;
        team->second.point += 3;
    }
    else if (diff == 0) {
        team->second.ties++;
        team->second.point++;
    }
    else
        team->second.lose++;

    team->second.n++;
    team->second.earn += earn;
    team->second.give += give;
    team->second.diff += diff;
}

static bool CompareRank(pair<string, TeamInfo> a, pair<string, TeamInfo> b) {
    auto infoA = a.second, infoB = b.second;
    if (infoA.point > infoB.point)
        return true;
    else if (infoA.point < infoB.point)
        return false;

    if (infoA.win > infoB.win)
        return true;
    else if (infoA.win < infoB.win)
        return false;

    if (infoA.diff > infoB.diff)
        return true;
    else if (infoA.diff < infoB.diff)
        return false;

    if (infoA.earn > infoB.earn)
        return true;
    else if (infoA.earn < infoB.earn)
        return false;

    if (infoA.n < infoB.n)
        return true;
    
    string upperNameA(a.first);
    string upperNameB(b.first);
    for (char &c : upperNameA)
        if ( 'a' <= c && c <= 'z' )
            c -= 'a' - 'A';
    for (char &c : upperNameB)
        if ( 'a' <= c && c <= 'z' )
            c -= 'a' - 'A';

    return strcmp(upperNameA.c_str(), upperNameB.c_str()) == -1;
}

int main(int argc, char const *argv[]) {
    /* code */
    string line;
    getline(cin, line);
    int nCase = stoi(line);

    for (int iCase = 0; iCase < nCase; ++iCase) {


        // get name of this tournament
        string tournamentName;
        getline(cin, tournamentName);
        cout << tournamentName << endl;

        // how many teams
        getline(cin, line);
        int nTeams = stoi(line);

        // team name and < wins, lose, ties, goals earned, goals lose >
        map<string, TeamInfo> teamInfo;

        // get teams' name
        string teamName;
        for (int i = 0; i < nTeams; ++i) {
            getline(cin, teamName);
            teamInfo.insert(make_pair(teamName, TeamInfo()));
        }

        // how many tournaments
        getline(cin, line);
        int nTournament = stoi(line);

        // get tournaments' info
        int pos, goalA, goalB;
        string nameA, nameB;
        for (int i = 0; i < nTournament; ++i) {
            getline(cin, line);

            // parse this line, teamA#goalsA@goalsB#teamB
            pos = line.find("#");
            nameA = line.substr(0, pos);
            line.erase(0, pos + 1);

            pos = line.find("@");
            goalA = stoi(line.substr(0, pos));
            line.erase(0, pos + 1);

            pos = line.find("#");
            goalB = stoi(line.substr(0, pos));
            line.erase(0, pos + 1);

            nameB = line;

            LogGame(teamInfo, nameA, goalA, goalB);
            LogGame(teamInfo, nameB, goalB, goalA);
        }

        // copy map to vector and sort it
        vector<pair<string, TeamInfo>> teamRank;
        for (auto pair : teamInfo)
            teamRank.push_back(pair);

        sort(teamRank.begin(), teamRank.end(), CompareRank);

        // sort tournaments
        for (int i = 0; i < nTeams; ++i) {
            auto info = teamRank[i].second;
            printf("%d) %s ", i + 1, teamRank[i].first.c_str());
            printf("%dp, %dg (%d-%d-%d), ", info.point, info.n, info.win, info.ties, info.lose);
            printf("%dgd (%d-%d)\n", info.diff, info.earn, info.give);
        }
        if (iCase != nCase - 1)
            cout << endl;
    }

    return 0;
}
