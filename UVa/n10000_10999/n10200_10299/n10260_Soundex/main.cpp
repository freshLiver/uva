#include <bits/stdc++.h>

using namespace std;

inline int printSoundex(char ch, int last) {

    int res;
    if (ch == 'R')
        res = 6;
    else if (ch == 'M' || ch == 'N')
        res = 5;
    else if (ch == 'L')
        res = 4;
    else if (ch == 'D' || ch == 'T')
        res = 3;
    else if (ch == 'C' || ch == 'G' || ch == 'J' || ch == 'K' || ch == 'Q' || ch == 'S' || ch == 'X' || ch == 'Z')
        res = 2;
    else if (ch == 'B' || ch == 'F' || ch == 'P' || ch == 'V')
        res = 1;
    else
        return -1;

    if (res != last)
        printf("%d", res);

    return res;
}


int main(int argc, char const *argv[]) {

    string line;
    while (getline(std::cin, line)) {
        int last = printSoundex(line[0], -1);
        for (int i = 1; i < line.length(); ++i)
            last = printSoundex(line[i], last);
        printf("\n");
    }

    return 0;
}
