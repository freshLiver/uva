#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    int cases;
    scanf("%d", &cases);

    for (int iCase = 0; iCase < cases; ++iCase) {

        int nDays, nParties;
        scanf("%d", &nDays);
        scanf("%d", &nParties);

        int intervals[nParties];
        for (int i = 0; i < nParties; ++i)
            scanf("%d", &intervals[i]);

        int count = 0;
        for (int iDay = 0; iDay < nDays; ++iDay) {
            if (iDay % 7 == 5 || iDay % 7 == 6)
                continue;
            else {
                bool hartals = false;
                for (int i = 0; !hartals && i < nParties; ++i)
                    hartals = !((iDay + 1) % intervals[i]);
                count += hartals;
            }
        }
        printf("%d\n", count);
    }

    return 0;
}
