#include <cstdio>
#include <cstring>
#include <iostream>
#include <string.h>

using namespace std;

int main(int argc, char const *argv[]) {

    for (int grps = 0;;) {

        // how many groups
        cin >> grps;
        if (grps <= 0)
            break;

        // get input string
        string str;
        cin >> str;

        // size of each groups
        int gSize = str.length() / grps;

        for (int i = 0; i < grps; ++i) {
            for (int j = 1; j <= gSize; ++j) {
                cout << str[(i + 1) * gSize - j];
            }
        }
        cout << endl;
    }

    return 0;
}
