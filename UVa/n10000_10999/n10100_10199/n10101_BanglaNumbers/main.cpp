#include <bits/stdc++.h>

using namespace std;

typedef long long int LL;

int main(int argc, char const *argv[]) {

    int take[] = {2, 1, 2, 2};

    string unit[] = {"shata", "hajar", "lakh", "kuti"};

    // input until EOF
    string line;
    int count = 1;
    while (getline(std::cin, line)) {

        // result list
        LL value = atoll(line.c_str());

        vector<string> results;
        char fmt[20];

        // 15 bits
        int v8 = value / 100000000000000;
        if (v8 != 0) {
            value = value % 100000000000000;
            sprintf(fmt, "%d kuti", v8);
            results.push_back(string(fmt));
        }

        // 13 bits
        int v7 = value / 1000000000000;
        if (v7 != 0) {
            value = value % 1000000000000;
            sprintf(fmt, "%d lakh", v7);
            results.push_back(string(fmt));
        }

        // 11 bits
        int v6 = value / 10000000000;
        if (v6 != 0) {
            value = value % 10000000000;
            sprintf(fmt, "%d hajar", v6);
            results.push_back(string(fmt));
        }

        // 10 bits
        int v5 = value / 1000000000;
        if (v5 != 0) {
            value = value % 1000000000;
            sprintf(fmt, "%d shata", v5);
            results.push_back(string(fmt));
        }

        // 8 bits
        int v4 = value / 10000000;
        if (v4 != 0) {
            value = value % 10000000;
            sprintf(fmt, "%d kuti", v4);
            results.push_back(string(fmt));
        }

        if (v4 == 0 && results.size())
            results[results.size() - 1 - !!v4] += " kuti";

        // 6 bits
        int v3 = value / 100000;
        if (v3 != 0) {
            value = value % 100000;
            sprintf(fmt, "%d lakh", v3);
            results.push_back(string(fmt));
        }

        // 4 bits
        int v2 = value / 1000;
        if (v2 != 0) {
            value = value % 1000;
            sprintf(fmt, "%d hajar", v2);
            results.push_back(string(fmt));
        }

        // 3 bits
        int v1 = value / 100;
        if (v1 != 0) {
            value = value % 100;
            sprintf(fmt, "%d shata", v1);
            results.push_back(string(fmt));
        }

        // 2 bits
        int v0 = value % 100;
        if (v0 != 0) {
            sprintf(fmt, "%d", v0);
            results.push_back(string(fmt));
        }

        // print label
        printf("%4d. ", count++);

        // print result
        if (results.size() != 0) {
            for (int i = 0; i < (results.size() - 1); ++i)
                printf("%s ", results[i].c_str());
            printf("%s\n", results.back().c_str());
        }
        else
            printf("0\n");
    }

    return 0;
}
