#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    string line;
    getline(cin, line);
    int nCase = stoi(line);

    for (int iCase = 0; iCase < nCase; ++iCase) {

        vector<string> current, answer;

        getline(cin, line);
        int nTurtle = stoi(line);

        // get current stack
        for (int i = 0; i < nTurtle; ++i) {
            getline(cin, line);
            current.push_back(line);
        }
        // get answer
        for (int i = 0; i < nTurtle; ++i) {
            getline(cin, line);
            answer.push_back(line);
        }


        int iAns = answer.size() - 1;
        bool found = false;
        for (int iCurr = current.size() - 1; iCurr >= 0; --iAns, --iCurr) {
            if (found = (current[iCurr] == answer[iAns]))
                continue;
            while (current[iCurr] != answer[iAns] && iCurr >= 0) {
                --iCurr;
                found = false;
            }
            found = (iCurr >= 0);
        }
        for (iAns += !found; iAns >= 0; --iAns)
            cout << answer[iAns] << endl;
        cout << endl;
    }

    return 0;
}
