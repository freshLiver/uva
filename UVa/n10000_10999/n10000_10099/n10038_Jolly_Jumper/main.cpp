#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {
    //
    int n, count = 0;
    while (EOF != scanf("%d", &n)) {

        ++count;

        int tokens[n];

        for (int i = 0, tmp; i < n; ++i) {
            scanf("%d", &tmp);
            tokens[i] = tmp;
        }

        if (n == 1) {
            printf("Jolly\n");
            continue;
        }

        bool inRange = true;
        int diff, sum = 0;
        for (int i = 1; i < n && inRange; ++i) {
            diff = abs(tokens[i] - tokens[i - 1]);
            sum += diff;
            inRange = (1 <= diff && diff < n);
        }

        if (inRange && sum == (n * (n - 1) / 2))
            printf("Jolly\n");
        else
            printf("Not jolly\n");
    }

    return 0;
}
