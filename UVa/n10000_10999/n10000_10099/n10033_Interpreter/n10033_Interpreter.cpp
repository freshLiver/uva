#include <cstdio>
#include <cstdlib>


using namespace std;

int doCommand( int reg[], int input, int pc ) {
    int d2 = input / 100;
    int d1 = ( input / 10 ) % 10;
    int d0 = input % 10;

    // parse this command
    switch ( d2 ) {
        case 0:
            // 0ds, if reg s != 0, goto reg d
            if ( reg[d0] != 0 )
                return reg[d1];
            else 
                return -1;
            break;

        case 1:
            // 100, halt
            break;

        case 2:
            // 2dn, set reg d to value n
            reg[d1] = d0;
            break;

        case 3:
            break;

        case 4:
            break;

        case 5:
            break;
        case 6:
            break;

        case 7:
            break;

        case 8:
            break;

        case 9:
            break;
    }
}


int main( int argc, char const *argv[] ) {

    // init 10 reg
    int reg[10] = { 0 };

    // input commands
    int input;
    while ( EOF != scanf( "%d", &input ) ) {
    }


    return 0;
}
