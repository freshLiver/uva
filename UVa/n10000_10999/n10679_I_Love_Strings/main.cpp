#include <bits/stdc++.h>
using namespace std;

int main(int argc, char const *argv[]) {

    int nCase;
    cin >> nCase;

    for (int iCase = 0; iCase < nCase; ++iCase) {

        // input sample string
        string sample;
        cin >> sample;

        // how many test strings
        int nTest;
        cin >> nTest;

        // input $nTest test strings
        for (int iTest = 0; iTest < nTest; ++iTest) {
            string test;
            cin >> test;

            // test this string
            // BUG !! TLE
            cout << (string::npos != sample.find(test) ? 'y' : 'n') << endl;
        }
    }

    return 0;
}
