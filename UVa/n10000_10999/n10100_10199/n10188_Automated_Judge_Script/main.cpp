#include <bits/stdc++.h>

using namespace std;

#define AC 0
#define PE 1
#define WA 2

int main(int argc, char const *argv[]) {
    /* code */


    for (int count = 1;; ++count) {

        string line;
        int ansLines, outLines;
        string answers, outputs;

        // input answers
        getline(cin, line);
        ansLines = stoi(line);

        if (ansLines == 0)
            break;

        for (int i = 0; i < ansLines; ++i) {
            getline(cin, line);
            answers += line + "\n";
        }

        // input user outputs
        getline(cin, line);
        outLines = stoi(line);

        for (int i = 0; i < outLines; ++i) {
            getline(cin, line);
            outputs += line + "\n";
        }

        // compare answer and user output
        int state = AC;

        // check answer and user output
        if (strcmp(answers.c_str(), outputs.c_str())) {
            // if wrong ans ( NUMERIC CHAR WRONG )
            vector<char> ansNums, outNums;

            for (char ch : answers)
                if ('0' <= ch && ch <= '9')
                    ansNums.push_back(ch);

            for (char ch : outputs)
                if ('0' <= ch && ch <= '9')
                    outNums.push_back(ch);

            // compare numbers
            if (ansNums.size() == outNums.size()) {
                for (int i = 0; i < ansNums.size(); ++i)
                    if (ansNums[i] != outNums[i]) {
                        state = WA;
                        break;
                    }
            }
            else
                state = WA;
            state = max(state, PE);
        }



        // output answer state
        if (state == AC)
            printf("Run #%d: %s\n", count, "Accepted");
        else if (state == PE)
            printf("Run #%d: %s\n", count, "Presentation Error");
        else
            printf("Run #%d: %s\n", count, "Wrong Answer");
    }
    return 0;
}
