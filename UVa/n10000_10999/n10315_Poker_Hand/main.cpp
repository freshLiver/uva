#include <bits/stdc++.h>

using namespace std;

#define Straight 6
#define Four 5
#define Full 4
#define Flush 3
#define Three 2
#define TwoPair 1
#define Two 0
#define High -1

int iCase = 0;

int GetValue(char number, char suit) {

    int value = 0;
    if ('2' <= number && number <= '9')
        value = 10 * (number - '0');
    else if (number == 'T')
        value = 100;
    else if (number == 'J')
        value = 110;
    else if (number == 'Q')
        value = 120;
    else if (number == 'K')
        value = 130;
    else
        value = 140;


    if (suit == 'S')
        value += 4;
    else if (suit == 'H')
        value += 3;
    else if (suit == 'D')
        value += 2;
    else
        value += 1;

    return value;
}

pair<int, int> getBestForm(int sorted[]) {

    int maxValue = sorted[4] / 10;

    bool isFlush = true;
    for (int i = 1; i < 5 && isFlush; ++i)
        if ((sorted[i] / 10) != (sorted[i - 1] / 10 + 1))
            isFlush = false;

    // special case for A 2 3 4 5
    bool isSpecial = true;
    int special[] = {2, 3, 4, 5, 14};
    for (int i = 0; i < 5 && isSpecial; ++i)
        isSpecial = (sorted[i] / 10 == special[i]);

    isFlush |= isSpecial;

    if (isFlush) {
        bool sameSuit = true;
        for (int i = 1; i < 5; ++i)
            if ((sorted[i - 1] % 10) != (sorted[i] % 10))
                sameSuit = false;
        return make_pair(sameSuit ? Straight : Flush, maxValue);
    }

    auto maxNumSame = make_pair(0, 0);
    for (int i = 0; i < 5; ++i) {
        int value = sorted[i] / 10;
        int numSame = 0;
        for (int j = 0; j < 5; ++j)
            if ((sorted[j] / 10) == value)
                ++numSame;

        if (numSame >= maxNumSame.first)
            maxNumSame = make_pair(numSame, value);
    }
    vector<int> others;
    for (int i = 0, count = 0; i < 5; ++i)
        if ((sorted[i] / 10) != maxNumSame.second)
            others.push_back(sorted[i]);

    if (maxNumSame.first == 4)
        return make_pair(Four, maxNumSame.second);

    else if (maxNumSame.first == 3) {
        // full
        if (others[0] / 10 == others[1] / 10)
            return make_pair(Full, maxValue);
        // three
        return make_pair(Three, maxValue);
    }
    else if (maxNumSame.first == 2) {
        // 2 pair
        if ((others[0] / 10) == (others[1] / 10) || (others[1] / 10) == (others[2] / 10))
            return make_pair(TwoPair, max(others[1] / 10, maxNumSame.second));
        // pair
        return make_pair(Two, maxNumSame.second);
    }
    return make_pair(High, maxValue);
}

// TODO compare pair
inline pair<int, vector<int>> RemoveLargerPair(vector<int> sorted) {

    bool isSingle[sorted.size()];
    for (int i = 0; i < sorted.size(); ++i)
        isSingle[i] = 1;

    int pairValue = -1;
    vector<int> remains;

    for (int i = 4; i > 0; --i)
        if (sorted[i] / 10 == sorted[i - 1] / 10) {
            isSingle[i] = 0;
            isSingle[i - 1] = 0;
            pairValue = sorted[i] / 10;
            break;
        }

    for (int i = 0; i < sorted.size(); ++i)
        if (isSingle[i])
            remains.push_back(sorted[i]);

    return make_pair(pairValue, remains);
}

void ComparePair(int blacks[], int whites[], int nPair) {

    vector<int> dupB(blacks, blacks + 5);
    vector<int> dupW(whites, whites + 5);

    auto remainBlacks = RemoveLargerPair(dupB);
    auto remainWhites = RemoveLargerPair(dupW);


    string res;
    if (remainBlacks.first > remainWhites.first)
        res = "Black wins.";
    else if (remainBlacks.first < remainWhites.first)
        res = "White wins.";
    else {
        if (nPair == TwoPair) {
            remainBlacks = RemoveLargerPair(remainBlacks.second);
            remainWhites = RemoveLargerPair(remainWhites.second);

            if (remainBlacks.first > remainWhites.first)
                res = "Black wins.";
            else if (remainBlacks.first < remainWhites.first)
                res = "White wins.";
            else {
                int maxB = remainBlacks.second.back() / 10;
                int maxW = remainWhites.second.back() / 10;
                if (maxB > maxW)
                    res = "Black wins.";
                else if (maxB < maxW)
                    res = "White wins.";
                else
                    res = "Tie.";
            }
        }
        else {

            // compare all single
            auto remBs = remainBlacks.second;
            auto remWs = remainWhites.second;

            bool same = true;
            for (int i = 2; i >= 0 && same; --i) {
                same = (remBs[i] / 10 == remWs[i] / 10);
                if (!same)
                    res = remBs[i] > remWs[i] ? "Black wins." : "White wins.";
            }
            if (same)
                res = "Tie.";
        }
    }
    printf("%s\n", res.c_str());
}


int main(int argc, char const *argv[]) {

    // repeat until EOF
    string line;
    while (getline(std::cin, line)) {

        ++iCase;

        // parse this line
        int blacks[5] = {0};
        int whites[5] = {0};

        for (int i = 0; i < 5; ++i)
            blacks[i] = GetValue(line[3 * i], line[3 * i + 1]);
        for (int i = 5; i < 10; ++i)
            whites[i - 5] = GetValue(line[3 * i], line[3 * i + 1]);

        // sort values
        sort(blacks, blacks + 5);
        sort(whites, whites + 5);

        // print result
        auto resB = getBestForm(blacks);
        auto resW = getBestForm(whites);

        if (iCase == 40)
            int a = 0;

        if (resB.first > resW.first)
            printf("Black wins.\n");
        else if (resB.first < resW.first)
            printf("White wins.\n");
        else {
            if (resB.first == High) {
                bool same = true;
                for (int i = 4; i >= 0 && same; --i) {
                    same = (blacks[i] / 10 == whites[i] / 10);
                    if (!same)
                        printf("%s wins.\n", blacks[i] > whites[i] ? "Black" : "White");
                }
                if (same)
                    printf("Tie.\n");
            }
            else if (resB.first == Two || resB.first == TwoPair)
                ComparePair(blacks, whites, resB.first);
            else {
                if (resB.second > resW.second)
                    printf("Black wins.\n");
                else if (resB.second < resW.second)
                    printf("White wins.\n");
                else
                    printf("Tie.\n");
            }
        }
    }
    return 0;
}
