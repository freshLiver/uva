#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    int nCase;
    cin >> nCase;

    for (int iCase = 0; iCase < nCase; ++iCase) {

        // input this case string
        string str;
        cin >> str;

        // find min period until half of string
        int minPeriod = str.length();
        for (int iChar = 1; iChar <= (str.length() + 1) / 2; ++iChar) {

            // substr len * n = str len, where n is uint
            if (str.length() % iChar != 0)
                continue;

            // get substring
            int repeats = str.length() / iChar;
            string substr = str.substr(0, iChar);

            // predict ans string
            char ans[str.length() + 1];
            for (int i = 0; i < repeats; ++i)
                memcpy(&ans[i * iChar], substr.c_str(), iChar);
            ans[str.length()] = '\0';

            // check ans
            if (!strcmp(str.c_str(), ans)) {
                minPeriod = iChar;
                break;
            }
        }
        cout << minPeriod << (iCase == nCase - 1 ? "\n" : "\n\n");
    }

    return 0;
}
