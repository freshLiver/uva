#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string.h>

using namespace std;

int isMine( const bool *field, const int rows, const int cols, const int pRow, const int pCol ) {

    // check range
    if ( pRow < 0 || pRow >= rows )
        return 0;
    if ( pCol < 0 || pCol >= cols )
        return 0;

    // check this position
    return !!( field[pRow * cols + pCol] );
}

// minesweepper
int main( int argc, char const *argv[] ) {

    // count field number
    int numFields = 0;
    string inputTmp;

    while ( getline( std::cin, inputTmp ) ) {

        // parse field size
        int rows, cols;
        sscanf( inputTmp.c_str(), "%d %d", &rows, &cols );

        // if row or col is 0, break
        if ( rows == 0 || cols == 0 )
            break;

        if ( inputTmp == "" )
            continue;

        // input mine positions
        ++numFields;
        bool field[rows][cols];

        // do $rows times getline to input all rows
        for ( int i = 0; i < rows; ++i ) {
            getline( std::cin, inputTmp );

            // check $cols chars in this line
            for ( int j = 0; j < cols; ++j )
                field[i][j] = ( inputTmp[j] == '*' );
        }

        // count mines around every block
        char result[rows][cols];

        for ( int row = 0; row < rows; ++row )
            for ( int col = 0; col < cols; ++col ) {

                // if this block is mine
                if ( field[row][col] )
                    result[row][col] = '*';

                // if this block is not mine, count mines around this block
                else {
                    int blockNum = cols * row + col;
                    int pos, count = 0;

                    // above 3
                    count += isMine( &field[0][0], rows, cols, row - 1, col - 1 );
                    count += isMine( &field[0][0], rows, cols, row - 1, col );
                    count += isMine( &field[0][0], rows, cols, row - 1, col + 1 );

                    // left and right
                    count += isMine( &field[0][0], rows, cols, row, col - 1 );
                    count += isMine( &field[0][0], rows, cols, row, col + 1 );

                    // under 3
                    count += isMine( &field[0][0], rows, cols, row + 1, col - 1 );
                    count += isMine( &field[0][0], rows, cols, row + 1, col );
                    count += isMine( &field[0][0], rows, cols, row + 1, col + 1 );


                    // set result
                    result[row][col] = count + '0';
                }
            }

        // print result
        printf( "%sField #%d:\n", numFields == 1 ? "" : "\n", numFields );
        for ( int i = 0; i < rows; ++i ) {
            for ( int j = 0; j < cols; ++j )
                printf( "%c", result[i][j] );
            printf( "\n" );
        }
    }

    return 0;
}
