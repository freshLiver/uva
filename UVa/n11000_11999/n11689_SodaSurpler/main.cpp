#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {

    // how many test cases
    int nCase;
    scanf("%d", &nCase);

    for (int iCase = 0; iCase < nCase; ++iCase) {

        int init, found, price;
        scanf("%d %d %d", &init, &found, &price);

        int total = 0;
        int remain = init + found;

        while (remain >= price) {
            int add = remain / price;
            remain = remain % price + add;
            total += add;
        }

        printf("%d\n", total);
    }
    return 0;
}
