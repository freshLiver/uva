#include <bits/stdc++.h>

using namespace std;


int main(int argc, char const *argv[]) {

    for (string a, b; getline(cin, a);) {
        getline(cin, b);

        char letters_a[26] = {0}, letters_b[26] = {0};
        // compute letters frequency of a
        for (char c : a)
            ++letters_a[c - 'a'];

        // compute letters frequency of b
        for (char c : b)
            ++letters_b[c - 'a'];


        // find common and min frequency
        for (int i = 0; i < 26; ++i)
            for (int j = 0; j < min(letters_a[i], letters_b[i]); ++j)
                printf("%c", i + 'a');

        cout << endl;
    }
    return 0;
    // UVa WA
}
