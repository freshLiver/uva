#include <bits/stdc++.h>

using namespace std;

int main( int argc, char const *argv[] ) {

    // get line until eof or break
    for ( int students = 0; EOF != scanf( "%d", &students ); ) {

        // student 0 for end
        if ( students == 0 )
            return 0;

        // get cost of each student
        vector<double> costs;
        double sum = 0;

        for ( int i = 0; i < students; ++i ) {
            double cost = 0;
            scanf( "%lf", &cost );
            cost = round( 100 * cost ) / 100.0;
            costs.push_back( cost );
            sum += cost;
        }

        // count avg
        double avg = round( 100 * ( sum / students ) ) / 100;
        double remain = 0, over = 0;

        for ( auto iter = costs.begin(); iter != costs.end(); ++iter ) {
            if ( *iter <= avg )
                remain += ( avg - *iter );
            else
                over += ( *iter - avg );
        }

        printf( "$%.2lf\n", min( remain, over ) );
    }

    return 0;
}
