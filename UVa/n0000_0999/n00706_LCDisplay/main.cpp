#include <bits/stdc++.h>

using namespace std;




int main(int argc, char const *argv[]) {

    // read size and value until size is 0
    while (true) {

        int size;
        string value;
        scanf("%d %s", &size, &value);

        if (size == 0)
            break;

        // 3 + size*2 rows
        for (int iRow = 0; iRow < 3 + size * 2; ++iRow) {

            // print each char
            for (int iChar = 0; iChar < value.size(); ++iChar) {

                // space between char
                printf(" ");
            }

            // new line
            printf("\n");
        }
    }

    return 0;
}
