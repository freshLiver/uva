#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {
    //

    while (true) {

        int size;
        scanf("%d", &size);

        if (size == 0)
            break;

        bool matrix[size][size];

        for (int c = 0; c < size; ++c)
            for (int r = 0, tmp; r < size; ++r) {
                scanf("%d", &tmp);
                matrix[r][c] = tmp;
            }


        bool ColSumOfRow[size] = {0};
        bool RowSumOfCol[size] = {0};

        for (int r = 0; r < size; ++r) {
            int sum = 0;
            for (int i = 0; i < size; ++i)
                sum += matrix[r][i];
            ColSumOfRow[r] = sum % 2;
        }

        for (int c = 0; c < size; ++c) {
            int sum = 0;
            for (int i = 0; i < size; ++i)
                sum += matrix[i][c];
            RowSumOfCol[c] = sum % 2;
        }

        int SumSumR = 0, SumSumC = 0;
        int wrongC, wrongR;
        for (int i = 0; i < size; ++i) {
            if (ColSumOfRow[i]) {
                wrongC = i;
                SumSumR += 1;
            }
            if (RowSumOfCol[i]) {
                wrongR = i;
                SumSumC += 1;
            }
        }

        if ( SumSumR ==0 && SumSumC == 0)
            printf("OK\n");
        else if ( SumSumC == 1 && SumSumR == 1)
            printf("Change bit (%d,%d)\n", wrongR + 1, wrongC + 1);
        else
            printf("Corrupt\n");
    }

    return 0;
}
